## Example of selenium java test automation


### Installation instructions: 

_Requirements:_

    java-se-11, maven,  chrome and firefox browsers.

### Install java:
https://java.com/en/download/help/download_options.xml

### Install maven:
https://maven.apache.org/install.html


Download chrome driver for chrome https://chromedriver.chromium.org/downloads

Download gecko driver for firefox https://github.com/mozilla/geckodriver/releases

Unpack both executables to directory in $PATH  ( %PATH%  for windows) 


### How to run:

Clone this repo.
Using command line execute 

`$ mvn clean test`

in the cloned repo directory.

Maven should download all dependencies, build and execute tests.

Human-readable report can be found after execution in 
./target/surefire-reports/index.html

#### Test suites:

* Negative Login test suite- parametrized to use incorrect/correct username, incorrect password and runs in chrome and firefox

* Negative search test suite- parametrized to run symbols ';'  '#' '^' '*' ')' runs in chrome. 

* Positive test suite - runs in chrome.





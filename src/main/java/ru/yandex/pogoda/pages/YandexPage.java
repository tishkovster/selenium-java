package ru.yandex.pogoda.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class YandexPage extends BasePageObject {

	public YandexPage(WebDriver driver, Logger log) {
		super(driver, log);
	}

	public String getPageUrl() {
		return driver.getCurrentUrl();
	}
}

package ru.yandex.pogoda.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePageObject {

	private By usernameLocator = By.id("passp-field-login");
	private By passwordLocator = By.id("passp-field-passwd");
	private By errorMessageLocator = By.cssSelector(
			".passp-form-field.passp-form-field_failed.passp-form-field_password > .passp-form-field__error"); 
	private By logInButtonLocator = By.xpath(("//button[@type='submit']"));

	public LoginPage(WebDriver driver, Logger log) {
		super(driver, log);
	}

	/** Execute log in */
	public SecureAreaPage logIn(String username, String password) {
		log.info("Executing LogIn with username [" + username + "] and password [" + password + "]");
		type(username, usernameLocator);
		click(logInButtonLocator);
		type(password, passwordLocator);
		click(logInButtonLocator);
		return new SecureAreaPage(driver, log);
	}

	public void negativeLogIn(String username, String password) {

		log.info("Executing LogIn with username [" + username + "] and password [" + password + "]");
		type(username, usernameLocator);
		click(logInButtonLocator);
		type(password, passwordLocator);
		click(logInButtonLocator);

	}

	public void waitForErrorMessage() {
		waitForVisibilityOf(errorMessageLocator, 5);

	}

	public String getErrorMessageText() {
		return find(errorMessageLocator).getText();
	}

}

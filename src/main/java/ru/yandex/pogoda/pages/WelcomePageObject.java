package ru.yandex.pogoda.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class WelcomePageObject extends BasePageObject {

	private String pageUrl = "https://yandex.ru/pogoda/";

	// private By formAuthenticationLinkLocator = By.linkText("Войти");
	private By formAuthenticationLinkLocator = By.className("login-button__text");
	private By yandexLogoLocator = By.xpath("//a[@href='//yandex.ru']");
	private By monthForecastLocator = By.cssSelector("a:nth-of-type(2) > span[role='menuitem']");
	private By weatherMapLocator = By.cssSelector("a:nth-of-type(3) > span[role='menuitem']");
	private By searchBar = By.id("header2input");

	public WelcomePageObject(WebDriver driver, Logger log) {
		super(driver, log);
	}

	/** Open PogodaPage */
	public void openPage() {
		log.info("Opening page: " + pageUrl);
		openUrl(pageUrl);
		log.info("Page opened!");
	}

	/** Open LoginPage by clicking on Form Authentication Link */
	public LoginPage clickFormAuthenticationLink() {
		log.info("Clicking Form Authentication link on Welcome Page");
		click(formAuthenticationLinkLocator);
		return new LoginPage(driver, log);
	}

	/** Open Yandex main page */
	public YandexPage clickYandexLogo() {
		log.info("Clicking on Yandex logo");
		click(yandexLogoLocator);
		return new YandexPage(driver, log);
	}

	/** Open Month Forecast page */
	public MonthForecastPage clickMonthForecast() {
		log.info("Clicking on Month Forecast");
		click(monthForecastLocator);
		return new MonthForecastPage(driver, log);

	}

	public WeatherMapPage clickWeatherMap() {
		log.info("Clicking on Weather Map");
		click(weatherMapLocator);
		return new WeatherMapPage(driver, log);
	}

	public SearchPage search(String searchString) {
		log.info("Executing search with search string '" + searchString + "' ");
		type(searchString, searchBar);
		typeKeys(Keys.ENTER, searchBar);
		return new SearchPage(driver, log);
	}

}

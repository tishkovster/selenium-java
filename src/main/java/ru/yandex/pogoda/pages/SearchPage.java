package ru.yandex.pogoda.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchPage extends BasePageObject {

	private By resultTitle = By.cssSelector(".title.title_level_1");

	public SearchPage(WebDriver driver, Logger log) {
		super(driver, log);

	}

	public boolean isResultTextVisible() {
		return find(resultTitle).isDisplayed();
	}

	public String resultText() {
		return find(resultTitle).getText();
	}

}

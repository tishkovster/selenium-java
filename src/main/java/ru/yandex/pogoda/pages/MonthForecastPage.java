package ru.yandex.pogoda.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MonthForecastPage extends BasePageObject {

	private By monthWeatherDiagram = By.xpath("//div[@class='climate-month-additional-diagram__image']");

	public MonthForecastPage(WebDriver driver, Logger log) {
		super(driver, log);
	}

	/** Verification if logOutButton is visible on the page */
	public boolean ismonthWeatherDiagramVisible() {
		return find(monthWeatherDiagram).isDisplayed();

	}
}

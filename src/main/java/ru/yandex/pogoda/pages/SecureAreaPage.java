package ru.yandex.pogoda.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SecureAreaPage extends BasePageObject {

	private String pageUrl = "yandex.ru";

	private By logOutButton = By.xpath("//div[@id='content']//a[@href='/logout']/i[@class='icon-2x icon-signout']");
	private By message = By.id("flash-messages");

	public SecureAreaPage(WebDriver driver, Logger log) {
		super(driver, log);
	}

	/** Get URI variable from PageObject */
	public String getPageUrl() {
		return pageUrl;
	}

	/** Verification if logOutButton is visible on the page */
	public boolean isLogOutButtonVisible() {
		return find(logOutButton).isDisplayed();
	}

	public String getSuccessMessageText() {
		return find(message).getText();
	}
}

package ru.yandex.pogoda.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WeatherMapPage extends BasePageObject {

	private By weatherMap = By.xpath("//div[@class='weather-maps__map'");

	public WeatherMapPage(WebDriver driver, Logger log) {
		super(driver, log);

	}

	public boolean isWeatherMapVisible() {
		return find(weatherMap).isDisplayed();

	}
}

package ru.yandex.pogoda.pogodapagetests;

import org.apache.commons.codec.binary.StringUtils;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import ru.yandex.pogoda.TestUtilities;
import ru.yandex.pogoda.pages.SearchPage;
import ru.yandex.pogoda.pages.WelcomePageObject;

public class NegativeSearchTests extends TestUtilities {

	// @Parameters({ "", "password", "expectedMessage" })
	@Test(dataProvider = "getData")
	public void negativeSeachTest(String param) {
		String expectedMessage = "По вашему запросу ничего не нашлось";
		byte[] bytes = StringUtils.getBytesUtf8(expectedMessage);
		String utf8EncodedString = StringUtils.newStringUtf8(bytes);

		log.info("Starting negativeSearchTest");
		// open main page
		WelcomePageObject welcomePage = new WelcomePageObject(driver, log);
		welcomePage.openPage();

		SearchPage searchpage = welcomePage.search(param);
		String resultText = searchpage.resultText();
		Assert.assertTrue(searchpage.isResultTextVisible(), "No search results should be displayed");
		Assert.assertEquals(resultText, utf8EncodedString,
				String.format("After searching for '%s' response message is: '%s' . But should be '%s'", param,
						resultText, utf8EncodedString));

	}

	@DataProvider
	public Object[][] getData(ITestContext context) {
		String parameter = context.getCurrentXmlTest().getLocalParameters().get("searchChar");
		String[] names = parameter.split(",");
		Object[][] returnValues = new Object[names.length][1];
		int index = 0;
		for (Object[] each : returnValues) {
			each[0] = names[index++].trim();
		}
		return returnValues;
	}

}
package ru.yandex.pogoda.pogodapagetests;

import org.testng.Assert;
import org.testng.annotations.Test;

import ru.yandex.pogoda.TestUtilities;
import ru.yandex.pogoda.pages.MonthForecastPage;
import ru.yandex.pogoda.pages.WeatherMapPage;
import ru.yandex.pogoda.pages.WelcomePageObject;
import ru.yandex.pogoda.pages.YandexPage;

public class PositiveTests extends TestUtilities {

	@Test
	public void navigateToMainYandexPageTest() {
		log.info("Starting Navigate to main Yandex page test");

		WelcomePageObject welcomePage = new WelcomePageObject(driver, log);
		welcomePage.openPage();
		YandexPage yandexPage = welcomePage.clickYandexLogo();
		Assert.assertEquals(yandexPage.getCurrentUrl(), "https://yandex.ru/",
				"Default Yandex should be displayed:" + yandexPage.getCurrentUrl());
	}

	@Test
	public void navigateToMonthWeatherForecast() {
		log.info("Starting Navigate to month Weather Forecast test");
		WelcomePageObject welcomePage = new WelcomePageObject(driver, log);
		welcomePage.openPage();
		MonthForecastPage monthForecastPage = welcomePage.clickMonthForecast();
		Assert.assertTrue(monthForecastPage.ismonthWeatherDiagramVisible(), "Month weather diagram is not presented");

	}

	@Test
	public void navigateToWeatherMap() {
		log.info("Starting: Navigate to Weather Map test");
		WelcomePageObject welcomePage = new WelcomePageObject(driver, log);
		welcomePage.openPage();
		WeatherMapPage weatherMapPage = welcomePage.clickWeatherMap();
		Assert.assertTrue(weatherMapPage.getCurrentUrl().contains("map"));
		// Assert.assertTrue(weatherMapPage.isWeatherMapVisible(), "Weather map is not
		// presented");
	}

}
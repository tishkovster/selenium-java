package ru.yandex.pogoda.pogodapagetests;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.pogoda.TestUtilities;
import ru.yandex.pogoda.pages.LoginPage;
import ru.yandex.pogoda.pages.WelcomePageObject;

public class NegativeLogInTests extends TestUtilities {

	@Parameters({ "username", "password" })
	@Test(priority = 1)
	public void negativeTest(String username, String password) {
		log.info("Starting negativeTest");

		// open main page
		WelcomePageObject welcomePage = new WelcomePageObject(driver, log);
		welcomePage.openPage();
		// open login page
		LoginPage loginPage = welcomePage.clickFormAuthenticationLink();

		// attempt to login
		loginPage.negativeLogIn(username, password);
		loginPage.waitForErrorMessage();

		String message = loginPage.getErrorMessageText();

		Assert.assertTrue(message.contains("Неверный пароль"),
				String.format("Message is: %s . But should be неверный пароль", message));

	}
}